from setuptools import setup

from wallpapertags import __version__, __author__, __email__

with open("README.md") as rm:
    long_desc = rm.read()
with open("requirements.txt") as req:
    requirements = req.read().splitlines()

setup(
    name = "wallpapertags",
    version = __version__,
    author = __author__,
    author_email = __email__,
    description = "Package for organizing and tagging your wallpapers",
    long_description=long_desc,
    license='GPL',
    #url='',
    packages=['wallpapertags'],
    install_requires=requirements,
    entry_points={
        'console_scripts': ['wallpapertags=wallpapertags.base:main'],
        'gui_scripts': ['wallpapertags-gui=wallpapertags.gui.main:main']
        },
)
