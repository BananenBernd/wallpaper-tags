import argparse

import context
from wallpapertags import database

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", dest="path_wallpaper")
    args = vars(parser.parse_args())
    args = {k: v for k, v in args.items() if v is not None}
    database.update_database(**args)

if __name__ == "__main__":
    main()
