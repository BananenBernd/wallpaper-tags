import argparse

import context
from wallpapertags import database

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", dest="path", default=database.FILE_DATABASE)
    args = vars(parser.parse_args())
    args = {k: v for k, v in args.items() if v is not None}
    data = database._read_database(**args)
    return data

if __name__ == "__main__":
    data = main()
