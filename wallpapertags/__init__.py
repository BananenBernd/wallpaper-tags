import logging

from .constants import *

logging.basicConfig(format="%(asctime)s - [%(levelname)8s]: %(message)s")

__author__ = "Karl Besser"
__email__ = "besser.karl@googlemail.com"
__version__ = "0.1"
