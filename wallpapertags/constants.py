import os

PATH_WALLPAPER = os.path.expanduser(os.path.join("~", "Pictures", "wallpapers"))
PATH_CONFIG = os.path.expanduser(os.path.join("~", ".config", "wallpapertags"))
os.makedirs(PATH_CONFIG, exist_ok=True)

FILE_DATABASE = "wallpapers.csv"

FILETYPES_IMAGE = ['.jpg', '.png', '.jpeg']
