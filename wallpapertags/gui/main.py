import sys

from PyQt5 import QtCore, QtGui, QtWidgets

from .gui_main import Ui_MainWindow

from .. import database


class GuiWindow(Ui_MainWindow):
    def setupUi(self, main_window):
        Ui_MainWindow.setupUi(self, main_window)
        #Signal connections
        ## GUI elements
        self.listWidget_filelist.itemSelectionChanged.connect(self._change_image_selection)
        self.pushButton_info_update_info.clicked.connect(self._update_info)
        ## Menu actions
        self.actionSave_database.triggered.connect(self._write_database)
        self.actionReload.triggered.connect(self._load_database)
        #self.graphics_scene = QtWidgets.QGraphicsScene()
        #self.graphicsView_main_view.setScene(self.graphics_scene)
        self.data = self.load_wallpaper_database()

    def _write_database(self):
        path = database.FILE_DATABASE
        database.write_database(path, self.data)

    def _load_database(self):
        path = database.FILE_DATABASE
        self.data = self.load_wallpaper_database(path)

    def load_wallpaper_database(self, path_database=database.FILE_DATABASE):
        data = database.read_database(path_database)
        self._set_filelist_from_data(data)
        return data

    def _set_filelist_from_data(self, data):
        self.listWidget_filelist.clear()
        self.listWidget_filelist.addItems(data['path'])

    def _change_image_selection(self):
        path = self.listWidget_filelist.currentItem().text()
        tags = self.data[self.data['path'] == path]['tags'].values[0]
        height, width = database.get_image_information(path)[1:3]
        self.label_info_dimensions.setText("{} x {}".format(height, width))
        self.label_info_ratio.setText("{:.3f}".format(height/width))
        self.lineEdit_info_tags.setText(tags)
        _pixmap = QtGui.QPixmap(path)
        _pixmap = _pixmap.scaled(self.graphicsView_main_view.size(), QtCore.Qt.KeepAspectRatio)
        _scene = QtWidgets.QGraphicsScene(self.graphicsView_main_view)
        self.graphicsView_main_view.setScene(_scene)
        _scene.clear()
        _scene.addPixmap(_pixmap)

    def _update_info(self):
        new_tags = self.lineEdit_info_tags.text()
        path = self.listWidget_filelist.currentItem().text()
        self.data.loc[self.data['path'] == path, 'tags'] = new_tags


def main():
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = GuiWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
