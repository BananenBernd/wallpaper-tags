import os
import csv
import logging

import pandas as pd
from PIL import Image

from .constants import (PATH_CONFIG, PATH_WALLPAPER, FILETYPES_IMAGE,
                        FILE_DATABASE)

FILE_DATABASE = os.path.join(PATH_CONFIG, FILE_DATABASE)
COLUMNS = ['path', 'width', 'height', 'tags']

__LOGGER = logging.getLogger(__name__)
__LOGGER.setLevel(logging.DEBUG)

def get_all_wallpapers(path=PATH_WALLPAPER,
                       filetypes=FILETYPES_IMAGE):
    wallpapers_gen = os.walk(path)
    wallpapers = []
    for dirpath, dirnames, filenames in wallpapers_gen:
        wallpapers.extend([os.path.join(dirpath, k) for k in filenames
                           if os.path.splitext(k)[1] in filetypes])
    return wallpapers


def _update_database(path=FILE_DATABASE, path_wallpaper=PATH_WALLPAPER,
                     filetypes=FILETYPES_IMAGE, columns=COLUMNS, old_data=None):
    wallpapers = get_all_wallpapers(path_wallpaper, filetypes=filetypes)
    __LOGGER.debug("Found %i wallpapers", len(wallpapers))
    if old_data is None:
        old_data = pd.DataFrame([], columns=columns)
    new_data = []
    for wallpaper in wallpapers:
        if wallpaper in old_data['path'].values:
            continue
        try:
            row = get_image_information(wallpaper)
            new_data.append(row)
        except OSError as e:
            __LOGGER.error("Could not get information from image: %s\n%s",
                           wallpaper, e)
    new_data = pd.DataFrame(new_data, columns=columns)
    dataframe = pd.concat((old_data, new_data))
    write_database(path, dataframe)
    __LOGGER.info("Added %i wallpapers to database", len(new_data))

def update_database(path=FILE_DATABASE, path_wallpaper=PATH_WALLPAPER,
                    filetypes=FILETYPES_IMAGE, columns=COLUMNS):
    try:
        old_data = read_database(path, columns)
    except (FileNotFoundError, pd.errors.ParserError):
        _message = "Could not read database file! Creating a new one."
        print(_message)
        __LOGGER.error(_message)
        old_data = None
    _update_database(path, path_wallpaper, filetypes, columns, old_data)

def create_database(path=FILE_DATABASE, path_wallpaper=PATH_WALLPAPER,
                    filetypes=FILETYPES_IMAGE, columns=COLUMNS):
    _update_database(path, path_wallpaper, filetypes, columns, old_data=None)

def write_database(path, data):
    data.to_csv(path, sep=',', mode='w', index=False, quoting=csv.QUOTE_ALL)

def read_database(path, columns=COLUMNS):
    data = pd.read_csv(path, index_col=False, na_filter=False)
    return data


def get_image_information(path, tags=""):
    with Image.open(path) as img:
        width, height = img.size
    return path, width, height, tags


def set_tag_to_wallpaper(data, path, tag):
    data.loc[data['path'] == path, 'tags'] = tag
    return data
